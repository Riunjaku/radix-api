# GatewayPagamento (Cielo & Stone)

Um repositorio dedicado ao desafio da Radix.

### Documentação

Documentação feita com Swagger, para acessar basta conectar ao link + "/Swagger".

### Como buildar

git clone https://gitlab.com/Riunjaku/radix-api.git

Atualize a connection string no arquivo appSettings.cs no projeto api.commom

Utilize o comando update-database no console manager apontando para o projeto api.commom

### Especificação

Você foi contratado para implementar um gateway de pagamentos. Um Gateway de pagamentos é um sistema capaz de enviar requisições de compras, por exemplo cartão de crédito, para empresas processadoras de pagamentos, as chamadas adquirentes, e oferece ao lojista um unico ponto de integraçao para várias adquirentes, a vantagem de se usar um gateway é que com apenas um contrato de integração o lojista poderá se integrar com várias empresas de pagamentos e com sistemas antifraudes.

Sua missão é desenvolver um gateway especializado em e-commerce que poderá processar pagamentos de vários lojistas, cada lojista poderá ter contrato com um e apenas um adquirente e ainda poderá ter ou não contrato com sistemas anti-fraudes.


Requisitos:
1) Sua API deverá oferecer um contrato de utilização único que será disponibilizado para os lojistas que irão utilizar sua plataforma de pagamentos;


2) Você deverá se integrar com os seguintes adquirentes:
- Stone(http://gateway.stone.com.br/docs/bem-vindo)
- Cielo(https://developercielo.github.io/manual/cielo-ecommerce);

OBS: Você não precisa integrar de verdade com a API deles, mas sim poderá "mockar" os serviços de pagamentos respeitando o contrato dessas adquirentes. Você não precisa implementar o mock de todos os serviços dessas adquirentes, mas sim apenas o serviço de criação de transações;

3) Você deverá implementar um mock do serviço de antifraulde da Clear Sale(https://www2.clear.sale/developers/api#appendix-requestsend), lembrando que apenas o serviço de envio  de requisição do link;

4) Lojas poderão ou não ter contratos com antifraulde.

5) O fluxo do antifraulde deverá acontecer antes de enviar a transação para as adquirentes

 - Caso o status retornado do antifraulde(https://www2.clear.sale/developers/api#appendix-status-list) for diferente de "APA" o fluxo de pagamento deverá ser interrompido e sua API deverá retornar o contrato de erro para o usuário

6) Um loja poderá estar configurada para apenas um adquirente, podendo ou não, ter contrato com antifraulde

7) Todos os seus mocks deverão contemplar os cenários de transações com sucesso e transações com erro

8) Sua API dará suporte para apenas transações de cartão de crédito

9) Você deverá desenvolver a API usando a tecnologia .NET Core 2

10) Deverá usar um banco de dados para guardar os registros de transações, pode usar o banco que você quiser;

11) Sua API deverá ter um serviço para registrar uma transação de compra e consultar transações por loja

12) Sua API deverá seguir a especificação RestFul (https://github.com/Microsoft/api-guidelines)

13) Sua aplicação deverá ser disponibilizada para avaliação na plataforma Gitlab

14) Seu projeto deverá ter uma documentação no formato README no repositório com todas as instruções para buildar, executar e testar sua aplicação.


Boa Sorte!

### Testando

Ao iniciar o projeto, a tela do swagger irá aparecer, nela poderá verificar todos os models, requests e responses do sistema.
Incluido no projeto se encontra um arquivo .txt, com um modelo inicial para o envio do request de pagamento, para acelerar o processo de testes.

## Authors

* **Eduardo Miranda** [Riunjaku](https://github.com/Riunjaku)
