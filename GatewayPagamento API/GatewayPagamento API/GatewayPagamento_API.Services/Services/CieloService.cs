﻿using GatewayPagamento.API.Common.Models;
using GatewayPagamento.API.DataContracts.Requests;
using GatewayPagamento.API.DataContracts.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace GatewayPagamento.Services.Services
{
    public class CieloService
    {
        public PagamentoResponse PagarCartaoCreditoMock(PagamentoRequest request)
        {
            var response = criarTransacaoMock(request);
            return response;
        }

        private PagamentoResponse criarTransacaoMock(PagamentoRequest request)
        {

            var CieloRequest = criarRequest(request);

            var baseAddress = "https://apisandbox.cieloecommerce.cielo.com.br/1/sales";

            var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";
            http.Headers["MerchantId"] = "3c1bfdd9-80b7-43af-b3af-cdfde3cf5859";
            http.Headers["MerchantKey"] = "WTVFBCZYYRSJWVDDASDXSBJQSXNYZMBXBDFGIOUF";

            string parsedContent = JsonConvert.SerializeObject(CieloRequest);
            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);

            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = http.GetResponse();

            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            var CieloResponse = JsonConvert.DeserializeObject<JObject>(content);
            var pagamentoResponse = new PagamentoResponse()
            {
                Status = CieloResponse["Payment"]["Status"].ToString() != "1" ? "Recusado" : "Transação Efetuada",
                Mensagem = CieloResponse["Payment"]["ReturnMessage"].ToString(),
                PaymentId = CieloResponse["Payment"]["PaymentId"].ToString()
            };
            if (request.RetornarSucesso)
            {
                pagamentoResponse.Status = "Transação Efetuada";
                pagamentoResponse.Mensagem = "Operation Successful";
            }
            else
            {
                pagamentoResponse.Status = "Recusado";
                pagamentoResponse.Mensagem = "Blocked Card";
            }
            return pagamentoResponse;

        }

        private CieloRequestModel criarRequest(PagamentoRequest pagamentoRequest)
        {
            var CieloRequest = new CieloRequestModel()
            {
                MerchantOrderId = pagamentoRequest.NumeroPedido,
                Customer = new CieloCustomerModel()
                {
                    Name = pagamentoRequest.cartao.nomeCartao
                },
                Payment = new CieloPaymentModel()
                {
                    Type = "CreditCard",
                    Amount = pagamentoRequest.ValorEntrega + pagamentoRequest.ValorPedido,
                    Installments = pagamentoRequest.Parcelas,
                    CreditCard = new CieloCreditCardModel()
                    {
                        CardNumber = pagamentoRequest.cartao.numeroCartao,
                        Holder = pagamentoRequest.cartao.nomeCartao,
                        ExpirationDate = pagamentoRequest.cartao.mesExpiracao + "/20" + pagamentoRequest.cartao.anoExpiracao,
                        SecurityCode = pagamentoRequest.cartao.codigoSeguranca,
                        Brand = pagamentoRequest.cartao.bandeiraCartao
                    }
                }

            };
            return CieloRequest;
        }
    }
}
