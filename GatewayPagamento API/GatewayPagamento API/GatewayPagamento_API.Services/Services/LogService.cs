﻿using GatewayPagamento.API.Common.Entity;
using GatewayPagamento.API.DataContracts.Requests;
using GatewayPagamento.API.Repository;
using GatewayPagamento.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.Services.Services
{
    public class LogService : ILogService
    {
        private readonly Context _ctx;
        public LogService(Context ctx)
        {
            _ctx = ctx;
        }
        public LogPagamento CriarLog(Loja loja, PagamentoRequest request)
        {
            var log = new LogPagamento
            {
                IdLoja_FK = loja.IdLoja,
                IdAdquirente = loja.IdAdquirente,
                ValorItems = request.ValorPedido,
                ValorEnvio = request.ValorEntrega,
                NumeroPedido = request.NumeroPedido,
                DataCriacao = DateTime.Now,
                Parcelas = request.Parcelas,
                Status = "Processando"
            };

            return log;
        }
    }
}
