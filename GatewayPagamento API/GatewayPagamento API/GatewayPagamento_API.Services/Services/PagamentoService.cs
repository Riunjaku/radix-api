﻿using GatewayPagamento.API.Common.Enum;
using GatewayPagamento.API.DataContracts.Requests;
using GatewayPagamento.API.DataContracts.Responses;
using GatewayPagamento.API.Repository;
using GatewayPagamento.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Linq;

namespace GatewayPagamento.Services.Services
{
    public class PagamentoService : IPagamentoService
    {
        private readonly Context _ctx;

        public PagamentoService(Context ctx)
        {
            _ctx = ctx;
        }

        public PagamentoResponse PagarCartaoCredito(PagamentoRequest pagamentoRequest, EnumAdquirente adquirente)
        {
            try
            {
                var pagamentoResponse = new PagamentoResponse();
                switch (adquirente)
                {
                    //trocar para suas interfaces
                    case EnumAdquirente.CIELO:
                        pagamentoResponse = new CieloService().PagarCartaoCreditoMock(pagamentoRequest);
                        break;
                    case EnumAdquirente.STONE:
                        pagamentoResponse = new StoneService().PagarCartaoCreditoMock(pagamentoRequest);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                return pagamentoResponse;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
