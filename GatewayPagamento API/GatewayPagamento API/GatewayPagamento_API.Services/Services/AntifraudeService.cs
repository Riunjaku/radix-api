﻿using GatewayPagamento.API.Common.Enum;
using GatewayPagamento.API.DataContracts.Responses;
using GatewayPagamento.API.Repository;
using GatewayPagamento.Services.Interfaces;
using GatewayPagamento.Services.Requests;
using System.Collections.Generic;
using System.Linq;

namespace GatewayPagamento.Services.Services
{
    public class AntifraudeService : IAntifraudeService
    {
        private readonly Context _ctx;
        public AntifraudeService(Context ctx)
        {
            _ctx = ctx;
        }

        public bool ValidarDadosMock(AntifraudeRequest request)
        {
            var antiFraudeResponse = validarMock(request);
            if (antiFraudeResponse.Orders.Any(x => x.Status == EnumAntiFraudeStatus.APA))
                return true;
            else
                return false;
        }

        private AntifraudeResponse validarMock(AntifraudeRequest request)
        {
            if (!request.RetornarFraude)
            {
                return new AntifraudeResponse()
                {
                    TransactionID = "001",
                    Orders = new List<OrderStatus>()
                    {
                        new OrderStatus()
                        {
                            ID = "001",
                            Score = 99.99M,
                            Status = EnumAntiFraudeStatus.APA
                        }
                    }
                };
            }
            else
            {
                return new AntifraudeResponse()
                {
                    TransactionID = "001",
                    Orders = new List<OrderStatus>()
                    {
                        new OrderStatus()
                        {
                            ID = "001",
                            Score = 0,
                            Status = EnumAntiFraudeStatus.RPA
                        }
                    }
                };
            }
        }
    }
}
