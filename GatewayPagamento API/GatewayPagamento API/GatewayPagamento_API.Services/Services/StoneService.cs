﻿using GatewayApiClient;
using GatewayApiClient.DataContracts;
using GatewayApiClient.DataContracts.EnumTypes;
using GatewayPagamento.API.DataContracts.Requests;
using GatewayPagamento.API.DataContracts.Responses;
using GatewayPagamento.API.Repository;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace GatewayPagamento.Services.Services
{
    public class StoneService
    {
        private readonly Context _ctx;


        public PagamentoResponse PagarCartaoCreditoMock(PagamentoRequest pagamentoRequest)
        {
            var response = CriarTransacaoMock(pagamentoRequest);

            return response;
        }

        private PagamentoResponse CriarTransacaoMock(PagamentoRequest request)
        {
            var pagamentoResponse = new PagamentoResponse();
            var transaction = new CreditCardTransaction()
            {
                AmountInCents = (long)request.ValorEntrega + (long)request.ValorPedido,
                CreditCard = new CreditCard()
                {
                    CreditCardBrand = CreditCardBrandEnum.Visa,
                    CreditCardNumber = request.cartao.numeroCartao,
                    ExpMonth = request.cartao.mesExpiracao,
                    ExpYear = request.cartao.anoExpiracao,
                    HolderName = request.cartao.nomeCartao,
                    SecurityCode = request.cartao.codigoSeguranca
                },
                InstallmentCount = request.Parcelas
            };

            var saleRequest = new CreateSaleRequest()
            {
                CreditCardTransactionCollection = new Collection<CreditCardTransaction>(new CreditCardTransaction[] { transaction }),
                Order = new Order()
                {
                    OrderReference = request.NumeroPedido
                }
            };

            //Como não consegui uma chave de hml da stone irei mockar o retorno baseado no request
            var serviceClient = new GatewayServiceClient(Guid.Parse("F2A1F485-CFD4-49F5-8862-0EBC438AE923"), new Uri("https://transaction.stone.com.br/Sale"));

            //var httpResponse = serviceClient.Sale.Create(saleRequest);

            //var retorno = httpResponse.Response.CreditCardTransactionResultCollection.FirstOrDefault();

            //pagamentoResponse = new PagamentoResponse()
            //{
            //    Status = retorno.AcquirerReturnCode != "0" || retorno.AcquirerReturnCode != "1" ? "Transação Efetuada com sucesso" : "Recusado",
            //    Mensagem = retorno.AcquirerMessage,
            //    PaymentId = retorno.TransactionIdentifier
            //};

            if (request.RetornarSucesso)
            {
                pagamentoResponse = new PagamentoResponse()
                {
                    Status = "Transação Efetuada com sucesso",
                    Mensagem = "Ainda sou um mock porque não conseguiu uma chave de hml",
                    PaymentId = Guid.NewGuid().ToString()
                };
                return pagamentoResponse;
            }
            else
            {
                pagamentoResponse = new PagamentoResponse()
                {
                    Status = "Recusado",
                    Mensagem = "Ainda sou um mock porque não conseguiu uma chave de hml",
                    PaymentId = Guid.NewGuid().ToString()
                };
                return pagamentoResponse;
            }
        }

    }
}
