﻿using GatewayPagamento.API.Common.Enum;
using GatewayPagamento.API.DataContracts.Requests;
using GatewayPagamento.API.DataContracts.Responses;

namespace GatewayPagamento.Services.Interfaces
{
    public interface IPagamentoService
    {
        PagamentoResponse PagarCartaoCredito(PagamentoRequest pagamentoRequest, EnumAdquirente adquirente);
    }
}
