﻿using GatewayPagamento.Services.Requests;

namespace GatewayPagamento.Services.Interfaces
{
    public interface IAntifraudeService
    {
        bool ValidarDadosMock(AntifraudeRequest request);
    }
}
