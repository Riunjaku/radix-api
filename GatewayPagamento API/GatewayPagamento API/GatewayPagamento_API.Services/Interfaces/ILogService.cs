﻿using GatewayPagamento.API.Common.Entity;
using GatewayPagamento.API.DataContracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.Services.Interfaces
{
    public interface ILogService
    {
        LogPagamento CriarLog(Loja loja, PagamentoRequest request);
    }
}
