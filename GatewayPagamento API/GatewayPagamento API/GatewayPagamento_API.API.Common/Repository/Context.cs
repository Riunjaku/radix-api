﻿using GatewayPagamento.API.Common.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace GatewayPagamento.API.Repository
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        {

        }

        public DbSet<Loja> Lojas { get; set; }
        public DbSet<Credential> Credentials { get; set; }
        public DbSet<LogPagamento> LogPagamento { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            base.OnModelCreating(modelBuilder);
        }

    }
}
