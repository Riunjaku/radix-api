﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GatewayPagamento.API.Common.Migrations
{
    public partial class Logs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Log_pagamento",
                columns: table => new
                {
                    IdLogPagamento = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdLoja_FK = table.Column<int>(nullable: false),
                    IdAdquirente = table.Column<int>(nullable: false),
                    ValorItems = table.Column<long>(nullable: false),
                    ValorEnvio = table.Column<long>(nullable: false),
                    NumeroPedido = table.Column<string>(nullable: true),
                    DataCriacao = table.Column<DateTime>(nullable: false),
                    Parcelas = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Log_pagamento", x => x.IdLogPagamento);
                    table.ForeignKey(
                        name: "FK_Log_pagamento_Loja_IdLoja_FK",
                        column: x => x.IdLoja_FK,
                        principalTable: "Loja",
                        principalColumn: "IdLoja",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Log_pagamento_IdLoja_FK",
                table: "Log_pagamento",
                column: "IdLoja_FK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Log_pagamento");
        }
    }
}
