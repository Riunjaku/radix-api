﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GatewayPagamento.API.Common.Migrations
{
    public partial class Migrationinicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Credential",
                columns: table => new
                {
                    IdCredentials = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MerchantId = table.Column<string>(nullable: true),
                    MerchantKey = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Credential", x => x.IdCredentials);
                });

            migrationBuilder.CreateTable(
                name: "Loja",
                columns: table => new
                {
                    IdLoja = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NomeLoja = table.Column<string>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    IdCredential_FK = table.Column<int>(nullable: false),
                    IdAdquirente = table.Column<int>(nullable: false),
                    ContrataAntiFraude = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Loja", x => x.IdLoja);
                    table.ForeignKey(
                        name: "FK_Loja_Credential_IdCredential_FK",
                        column: x => x.IdCredential_FK,
                        principalTable: "Credential",
                        principalColumn: "IdCredentials",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Loja_IdCredential_FK",
                table: "Loja",
                column: "IdCredential_FK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Loja");

            migrationBuilder.DropTable(
                name: "Credential");
        }
    }
}
