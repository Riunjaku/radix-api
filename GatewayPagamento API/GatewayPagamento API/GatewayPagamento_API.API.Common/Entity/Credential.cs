﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GatewayPagamento.API.Common.Entity
{
    [Table("Credential")]
    public class Credential
    {
        [Key]
        public int IdCredentials { get; set; }
        public string MerchantId { get; set; }
        public string MerchantKey { get; set; }
    }
}
