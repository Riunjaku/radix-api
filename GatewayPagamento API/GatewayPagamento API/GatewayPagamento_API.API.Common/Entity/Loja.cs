﻿using GatewayPagamento.API.Common.Enum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GatewayPagamento.API.Common.Entity
{
    [Table("Loja")]
    public class Loja
    {
        [Key]
        public int IdLoja { get; set; }
        public string NomeLoja { get; set; }
        public string Token { get; set; } 
        public int IdCredential_FK { get; set; }
        public EnumAdquirente IdAdquirente { get; set; }
        public bool ContrataAntiFraude { get; set; }


        [ForeignKey("IdCredential_FK")]
        public virtual Credential Credentials { get; set; }
    }
}
