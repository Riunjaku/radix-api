﻿using GatewayPagamento.API.Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GatewayPagamento.API.Common.Entity
{
    [Table("Log_pagamento")]
    public class LogPagamento
    {
        [Key]
        public int IdLogPagamento { get; set; }
        public int IdLoja_FK { get; set; }
        public EnumAdquirente IdAdquirente { get; set; }
        public long ValorItems { get; set; }
        public long ValorEnvio { get; set; }
        public string NumeroPedido { get; set; }
        public DateTime DataCriacao { get; set; }
        public int Parcelas { get; set; }
        public string Status { get; set; }

        [ForeignKey("IdLoja_FK")]
        public virtual Loja Loja { get; set; }
    }
}
