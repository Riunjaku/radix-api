﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.API.Common.Models
{
    public class Cliente
    {
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string CEP { get; set; }
        public Phone Telefone { get; set; }
    }

    public class Phone
    {
        /// <summary>
        /// Area Code	
        /// </summary>
        public int AreaCode { get; set; }
        /// <summary>
        /// Telephone Number
        /// </summary>
        public int Number { get; set; }
    }

   
}
