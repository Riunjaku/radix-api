﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.API.Common.Models
{
    public class Cartao
    {
        public string numeroCartao { get; set; }
        public string nomeCartao { get; set; }
        public string codigoSeguranca { get; set; }
        public string bandeiraCartao { get; set; }
        public int mesExpiracao { get; set; }
        public int anoExpiracao { get; set; }
    }
}
