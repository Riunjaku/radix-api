﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.API.Common.Models
{
    public class Produto
    {
        /// <summary>
        /// Identificação do produto
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Nome do produto
        /// </summary>
        public string Nome { get; set; }
        /// <summary>
        /// Valor do produto
        /// </summary>
        public decimal Valor { get; set; }
        /// <summary>
        /// Quantidade
        /// </summary>
        public int Quantidade { get; set; }
    }
   
}
