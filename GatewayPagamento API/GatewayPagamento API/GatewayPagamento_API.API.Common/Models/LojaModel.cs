﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.API.Common.Models
{
    public class LojaModel
    {
        public int? idAdquirente { get; set; }
        public string nomeLoja { get; set; }
        public string MerchantKey { get; set; }
        public string MerchantId { get; set; }
        public bool? ContratarAntifraude { get; set; }
    }
}
