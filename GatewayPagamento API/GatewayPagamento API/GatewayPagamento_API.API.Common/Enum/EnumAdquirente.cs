﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace GatewayPagamento.API.Common.Enum
{
    public enum EnumAdquirente
    {
        [Description("Cielo")]
        CIELO = 1,
        [Description("Stone")]
        STONE
    }
}
