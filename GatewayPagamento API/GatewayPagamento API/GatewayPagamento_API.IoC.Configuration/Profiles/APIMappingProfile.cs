﻿using AutoMapper;
using DC = GatewayPagamento_API.API.DataContracts;
using S = GatewayPagamento_API.Services.Model;

namespace GatewayPagamento_API.IoC.Configuration.Profiles
{
    public class APIMappingProfile : Profile
    {
        public APIMappingProfile()
        {
            CreateMap<S.User, DC.User>();
            CreateMap<S.Adress, DC.Adress>();
        }
    }
}
