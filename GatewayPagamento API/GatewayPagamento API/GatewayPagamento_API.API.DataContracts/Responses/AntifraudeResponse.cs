﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.API.DataContracts.Responses
{
    public class AntifraudeResponse
    {
        public List<OrderStatus> Orders { get; set; }
        public string TransactionID { get; set; }
    }
    public class OrderStatus
    {
        public string ID { get; set; }
        public string Status { get; set; }
        public decimal Score { get; set; }
    }
}
