﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.API.DataContracts.Responses
{
    public class RetornoOperacao
    {
        public string Status { get; set; }
        public string Mensagem { get; set; }
    }
}
