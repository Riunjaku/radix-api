﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.API.DataContracts.Responses
{
    public class PagamentoResponse
    {
        public string Status { get; set; }
        public string Mensagem { get; set; }
        public string PaymentId { get; set; }
    }
}
