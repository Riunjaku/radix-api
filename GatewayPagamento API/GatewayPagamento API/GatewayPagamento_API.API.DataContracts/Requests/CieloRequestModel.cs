﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.API.DataContracts.Requests
{
    public class CieloRequestModel
    {
        public string MerchantOrderId { get; set; }
        public CieloCustomerModel Customer { get; set; }
        public CieloPaymentModel Payment { get; set; }
    }

    public class CieloCustomerModel
    {
        public string Name { get; set; }
    }
    public class CieloPaymentModel
    {
        public string Type { get; set; }
        public long Amount { get; set; }
        public int Installments { get; set; }
        public CieloCreditCardModel CreditCard { get; set; }
    }
    public class CieloCreditCardModel
    {
        public string CardNumber { get; set; }
        public string Holder { get; set; }
        public string ExpirationDate { get; set; }
        public string SecurityCode { get; set; }
        public string Brand { get; set; }
    }
}
