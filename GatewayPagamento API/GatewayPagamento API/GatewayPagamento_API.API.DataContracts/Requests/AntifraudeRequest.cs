﻿using GatewayPagamento.API.DataContracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace GatewayPagamento.Services.Requests
{
    public class AntifraudeRequest
    {
        /// <summary>
        /// Chave de autenticação da API do clearSale
        /// </summary>
        public string ApiKey { get; set; }
        /// <summary>
        /// Token de login gerado pela ClearSale
        /// </summary>
        public string LoginToken { get; set; }
        public Order Orders { get; set; }
        /// <summary>
        /// Location of Analysis ( “BRA” to Brazil and “USA” to United States)
        /// </summary>
        public string AnalysisLocation { get; set; }
        /// <summary>
        /// Verificação do retorno do mock 
        /// </summary>
        public bool RetornarFraude { get; set; }


        public static AntifraudeRequest CriarMockRequest(PagamentoRequest request)
        {
            var antifraudeRequest = new AntifraudeRequest()
            {
                ApiKey = "Mock",
                AnalysisLocation = "BRA",
                LoginToken = "Mock",
                Orders = new Order()
                {
                    ID = "001",
                    Date = DateTime.Now,
                    Email = "Mock@Mock.com",
                    TotalItems = request.ValorPedido,
                    TotalOrder = request.ValorEntrega,
                    IP = "Mock",
                    Currency = "BRL",
                    Payments = new List<Payment>()
                    {
                        new Payment()
                        {
                            Date = DateTime.Now,
                            Amount = request.ValorEntrega + request.ValorPedido,
                            Type = 1
                        }
                    },
                    ShippingData = new Person()
                    {
                        ID = 1,
                        Type = 1,
                        Name = request.cartao.nomeCartao,
                        Address = new Address()
                        {
                            City = request.Cliente.Cidade,
                            State = request.Cliente.UF,
                            ZipCode = request.Cliente.CEP
                        },
                        Phones = new List<Phone>()
                        {
                            new Phone()
                            {
                                Type = 0,//Undefined,
                                AreaCode = request.Cliente.Telefone.AreaCode,
                                Number = request.Cliente.Telefone.Number
                            }
                        }
                    },
                    BillingData = new Person()
                    {
                        ID = 1,
                        Type = 1,
                        Name = request.cartao.nomeCartao,
                        Address = new Address()
                        {
                            City = request.Cliente.Cidade,
                            State = request.Cliente.UF,
                            ZipCode = request.Cliente.CEP
                        },
                        Phones = new List<Phone>()
                        {
                            new Phone()
                            {
                                Type = 0,//Undefined,
                                AreaCode = request.Cliente.Telefone.AreaCode,
                                Number = request.Cliente.Telefone.Number
                            }
                        }
                    },
                    Items = new List<Item>()
                    {
                        new Item()
                        {
                            ID = request.Produto.ID,
                            Name = request.Produto.Nome,
                            ItemValue = request.Produto.Valor,
                            Qty = request.Produto.Quantidade
                        }
                    },
                    Origin = "mock"
                },
                RetornarFraude = request.RetornarFraude

            };
            return antifraudeRequest;
        }
    }

    public class Order
    {
        /// <summary>
        /// Order Identification Code
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Order Date
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Order e-Mail
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Sum of Items Values
        /// </summary>
        public decimal TotalItems { get; set; }
        /// <summary>
        /// Order total Value
        /// </summary>
        public decimal TotalOrder { get; set; }
        /// <summary>
        /// Order originating IP address
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// Order currency (https://www2.clear.sale/developers/api#appendix-currency-codes)
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Payment Data	
        /// </summary>
        public List<Payment> Payments { get; set; }
        /// <summary>
        /// Shipping Data	
        /// </summary>
        public Person ShippingData { get; set; }
        /// <summary>
        /// Billing Data
        /// </summary>
        public Person BillingData { get; set; }
        /// <summary>
        /// Order Items	
        /// </summary>
        public List<Item> Items { get; set; }
        /// <summary>
        /// Order source
        /// </summary>
        public string Origin { get; set; }
    }

    public class Payment
    {
        /// <summary>
        /// Payment Date
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Payment Value
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 1	CREDIT CARD
        /// 2	PAYMENT SLIP
        /// 3	DIRECT DEBIT
        /// 4	AMAZON PAYMENTS
        /// 5	BITCOIN
        /// 6	BANK TRANSFER
        /// 7	APPLE PAY
        /// 8	CHECK
        /// 9	MONEY
        /// 10	FINANCING
        /// 11	INVOICE
        /// 12	CUPOM
        /// 13	PAYPAL
        /// 14	OTHER
        /// </summary>
        public int Type { get; set; }
    }

    public class Person
    {
        /// <summary>
        /// Customer Id	
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 1	Person
        /// 2	Company
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// Customer Name
        /// </summary>
        public string Name { get; set; }
        public Address Address { get; set; }
        public List<Phone> Phones { get; set; }
    }

    public class Address
    {
        /// <summary>
        /// City (No abbreviations)
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// UF
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// CEP
        /// </summary>
        public string ZipCode { get; set; }
    }

    public class Phone
    {
        /// <summary>
        /// 0	Undefined
        /// 1	Home Phone
        /// 2	Work Phone
        /// 3	Messages
        /// 4	Billing Phone
        /// 5	Temporary Phone
        /// 6	Mobile Phone
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// Area Code	
        /// </summary>
        public int AreaCode { get; set; }
        /// <summary>
        /// Telephone Number
        /// </summary>
        public int Number { get; set; }
    }

    public class Item
    {
        /// <summary>
        /// Product ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Product Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Unit Price
        /// </summary>
        public decimal ItemValue { get; set; }
        /// <summary>
        /// Quantity Purchased
        /// </summary>
        public int Qty { get; set; }
    }


}
