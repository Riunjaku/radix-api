﻿using System.ComponentModel.DataAnnotations;
using GatewayPagamento.API.Common.Models;

namespace GatewayPagamento.API.DataContracts.Requests
{
    public class PagamentoRequest
    {
        public string NumeroPedido { get; set; }
        public long ValorPedido { get; set; }
        public long ValorEntrega { get; set; }
        public int Parcelas { get; set; }
        public Cartao cartao { get; set; }
        public Cliente Cliente { get; set; } 
        public Produto Produto { get; set; }
        public bool RetornarFraude { get; set; }
        public bool RetornarSucesso { get; set; }
        [Required]
        public string Token { get; set; }

    }
}

