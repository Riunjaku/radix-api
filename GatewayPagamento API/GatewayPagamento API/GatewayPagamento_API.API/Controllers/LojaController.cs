﻿using GatewayPagamento.API.Common.Entity;
using GatewayPagamento.API.Common.Enum;
using GatewayPagamento.API.Common.Extensions;
using GatewayPagamento.API.Common.Models;
using GatewayPagamento.API.DataContracts.Responses;
using GatewayPagamento.API.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GatewayPagamento.API.Controllers
{
    public class LojaController : Controller
    {
        private readonly Context _ctx;

        public LojaController(Context ctx)
        {
            _ctx = ctx;
        }

        [Route("Register/CadastrarLoja")]
        [HttpPost]
        [ProducesResponseType(typeof(RetornoOperacao),200)]
        [ProducesResponseType(typeof(RetornoOperacao),400)]
        public IActionResult CadastrarLoja([FromBody]LojaModel lojaModel)
        {

            if (!Enum.IsDefined(typeof(EnumAdquirente), lojaModel.idAdquirente))
            {
                return BadRequest(new RetornoOperacao
                {
                    Status = "Erro",
                    Mensagem = "Adquirente Invalido."
                });
            }
            using (var tran = _ctx.Database.BeginTransaction())
            {
                try
                {
                    var credenciais = new Credential()
                    {
                        MerchantId = lojaModel.MerchantId,
                        MerchantKey = lojaModel.MerchantKey
                    };
                    _ctx.Credentials.Add(credenciais);
                    _ctx.SaveChanges();

                    var loja = new Loja()
                    {
                        NomeLoja = lojaModel.nomeLoja,
                        IdAdquirente = (EnumAdquirente)lojaModel.idAdquirente,
                        Token = Guid.NewGuid().ToString(),
                        IdCredential_FK = credenciais.IdCredentials
                    };
                    _ctx.Lojas.Add(loja);
                    _ctx.SaveChanges();

                    tran.Commit();

                    var response = new RetornoOperacao
                    {
                        Status = "Sucesso",
                        Mensagem = "Token= " + loja.Token
                    };

                    return Json(response);
                }
                catch (Exception e)
                {
                    return BadRequest(new RetornoOperacao
                    {
                        Status = "Erro",
                        Mensagem = e.Message
                    });
                }
            }
        }

        [Route("Register/MostrarDados")]
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(RetornoOperacao),400)]
        public IActionResult MostrarDados(string token)
        {
            var loja = _ctx.Lojas.Include(l => l.Credentials).
                FirstOrDefault(x => x.Token == token);
            if (loja == null)
                return BadRequest(new RetornoOperacao
                {
                    Status = "Erro",
                    Mensagem = "Token Invalido."
                });
            else
                return Json(new
                {
                    loja.IdLoja,
                    loja.NomeLoja,
                    loja.Token,
                    loja.ContrataAntiFraude,
                    //Trocar para getDescription
                    Adquirente = loja.IdAdquirente.Description(),
                    loja.Credentials.MerchantId,
                    loja.Credentials.MerchantKey
                });
        }

        [Route("Register/AtualizarLoja")]
        [HttpPost]
        [ProducesResponseType(typeof(RetornoOperacao),200)]
        [ProducesResponseType(typeof(RetornoOperacao),400)]
        public IActionResult AtualizarLoja([FromBody] string token, LojaModel lojaModel)
        {
            var loja = _ctx.Lojas.FirstOrDefault(x => x.Token == token);
            if (loja != null)
                return BadRequest(new RetornoOperacao
                {
                    Status = "Erro",
                    Mensagem = "Loja não encontrada."
                });
            else
            {
                if (!string.IsNullOrEmpty(lojaModel.nomeLoja))
                    loja.NomeLoja = lojaModel.nomeLoja;
                if (lojaModel.ContratarAntifraude.HasValue)
                    loja.ContrataAntiFraude = lojaModel.ContratarAntifraude.Value;
                if (lojaModel.idAdquirente.HasValue && string.IsNullOrEmpty(lojaModel.MerchantKey))
                {
                    if (!Enum.IsDefined(typeof(EnumAdquirente), lojaModel.idAdquirente))
                    {
                        return BadRequest(new RetornoOperacao
                        {
                            Status = "Erro",
                            Mensagem = "Adquirente Invalido."
                        });
                    }
                    if (lojaModel.idAdquirente == (int)EnumAdquirente.STONE)
                    {
                        loja.IdAdquirente = (EnumAdquirente)lojaModel.idAdquirente;
                        loja.Credentials.MerchantKey = lojaModel.MerchantKey;
                    }
                    else
                    {
                        loja.IdAdquirente = (EnumAdquirente)lojaModel.idAdquirente;
                        loja.Credentials.MerchantId = lojaModel.MerchantId;
                        loja.Credentials.MerchantKey = lojaModel.MerchantKey;
                    }
                }
            }

            return Json(new RetornoOperacao
            {
                Status = "Atualizado",
                Mensagem = "Informações atualizadas com sucesso."
            });
        }
    }
}