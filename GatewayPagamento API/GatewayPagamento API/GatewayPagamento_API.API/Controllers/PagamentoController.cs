﻿using GatewayPagamento.API.DataContracts.Requests;
using GatewayPagamento.API.DataContracts.Responses;
using GatewayPagamento.API.Repository;
using GatewayPagamento.Services.Interfaces;
using GatewayPagamento.Services.Requests;
using GatewayPagamento_API.API.Common.Attributes;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace GatewayPagamento.API.Controllers
{
    [TokenValidation]
    public class PagamentoController : Controller
    {
        private readonly Context _ctx;
        private readonly IAntifraudeService _antifraude;
        private readonly IPagamentoService _pagamentoService;
        private readonly ILogService _logService;

        public PagamentoController(IAntifraudeService antifraude, IPagamentoService pagamentoService, ILogService logService, Context ctx)
        {
            _antifraude = antifraude;
            _pagamentoService = pagamentoService;
            _logService = logService;
            _ctx = ctx;
        }

        [Route("Transactions/CapturaCartaoCredito")]
        [HttpPost]
        [ProducesResponseType(typeof(PagamentoResponse), 200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(typeof(RetornoOperacao),418)]
        public IActionResult RealizarPagamento([FromBody]PagamentoRequest request)
        {
            var loja = _ctx.Lojas.FirstOrDefault(x => x.Token == request.Token);
            if (loja == null)
            {
                return Unauthorized();
            }
            else if (loja.ContrataAntiFraude)
            {
                var antifraudeRequest = AntifraudeRequest.CriarMockRequest(request);
                if (!_antifraude.ValidarDadosMock(antifraudeRequest))
                {
                    return StatusCode(418,new RetornoOperacao
                    {
                        Status = "Erro",
                        Mensagem = "Possivel Fraude detectada."
                    });
                }

            }
            var log = _logService.CriarLog(loja, request);
            var response = _pagamentoService.PagarCartaoCredito(request,loja.IdAdquirente);

            log.Status = response.Status;

            _ctx.LogPagamento.Add(log);
            _ctx.SaveChanges();


            return Json(response);
        }
    }
}
